'use strict';

function Map(game, player, myGame) {
	this.myGame = myGame;
	this.player = player;
	this.game = game;
	this.sprites = [];
	this.tilemap = null;
	this.tileset = null;
	this.canvas = null;
	this.pix = null;
	this.threshold = 140;//200;//115;
	this.bottomLayer = null;
	this.collisionLayer = null;
	this.fogLayer = null;
	this.treeLayer = null;
	this.items = [];
};

Map.prototype = {

	create: function (data) {
		//this.game.stage.backgroundColor = '#7ec622';
		this.game.load.tilemap('map', null, data, Phaser.Tilemap.TILED_JSON );
		this.tilemap = this.game.add.tilemap('map');
		this.tilemap.addTilesetImage('grassdesert','grassdesert');

		//CrateLayers and bring groups to top.
		this.bottomLayer = this.tilemap.createLayer('bottomLayer');
		this.items = this.game.add.group();
		//this.items.bringToTop();
		this.myGame.survivorGroup = this.game.add.group();
		this.myGame.survivorGroup.createMultiple(100,'player');
		this.player.sprite.bringToTop();
		this.collisionLayer = this.tilemap.createLayer('collisionLayer');
		this.fogLayer = this.tilemap.createLayer('fogLayer');
		this.treeLayer = this.tilemap.createLayer('treeLayer');


		for(var y = 0; y < this.tilemap.height; y++) {
			for(var index = 0; index < this.tilemap.layers.length; index++){
				this.tilemap.layers[index].data[y] = [];
			}
			this.items[y] = [];
		}
		//this.treeLayer.resizeWorld();
	},

	update: function(mapData) {
		//console.log(mapData);
		for(var tileIndex = 0; tileIndex < mapData.tiles.length; tileIndex++) {
				var tileInfo = mapData.tiles[tileIndex];
				// {
				// 	x:x, 
				// 	y:y, 
				// 	layer:layerIndex, 
				// 	tile:this.mapData.layers[layerIndex].data[y][x]
				// }
				if(tileInfo.tile){
					this.tilemap.layers[tileInfo.layer].data[tileInfo.y ][tileInfo.x ] = new Phaser.Tile(
						this.tilemap.layers[tileInfo.layer], 
						tileInfo.tile,
						tileInfo.x,tileInfo.y,64,64);
					var tile = this.tilemap.layers[tileInfo.layer].data[tileInfo.y][tileInfo.x];
					if(tileInfo.layer > 0){
						tile.setCollision(true,true,true,true);
						tile.faceTop = true;
						tile.faceBottom = true;
						tile.faceLeft = true;
						tile.faceRight = true;
					}
					//console.log(tile.collides);
				}
		}

		//collision
		//change this to only do it around player
		for(var tileIndex = 0; tileIndex < mapData.tiles.length; tileIndex++) {
			var tileInfo = mapData.tiles[tileIndex];
			if(tileInfo.layer > 0){
				var tile = this.tilemap.layers[tileInfo.layer].data[tileInfo.y][tileInfo.x];
				if(tile){
					var above = this.tilemap.getTileAbove(tileInfo.layer, tileInfo.x, tileInfo.y);
					var below = this.tilemap.getTileBelow(tileInfo.layer, tileInfo.x, tileInfo.y);
					var left = this.tilemap.getTileLeft(tileInfo.layer, tileInfo.x, tileInfo.y);
					var right = this.tilemap.getTileRight(tileInfo.layer, tileInfo.x, tileInfo.y);
					if (above && above.collides)
						tile.faceTop = false;
					if (below && below.collides)
						tile.faceBottom = false;
					if (left && left.collides)
						tile.faceLeft = false;
					if (right && right.collides)
						tile.faceRight = false;
				}
			}
		}
		//TODO Code for recycling tiles when necessary...

		this.updateItems(mapData.items);
	},

	updateItems: function (items){
		// item
		// {
		// 	x:x,
		// 	y:y,
		// 	key:'items',//sprite key
		// 	//frame:optional //spriteset frame
		// 	id:1, //branch
		// 	name:'branch',
		// 	stats: [] //optional for armor, damage, speed, food
		// 				//{type: ___, value:___}
		// }
		for(var itemIndex = 0; itemIndex < items.length; itemIndex++) {
			var item = items[itemIndex];
			this.items.create(item.x * 64, item.y * 64, item.key, item.frame, true);
		}
	}
};