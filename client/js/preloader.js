(function() {
	'use strict';

	function Preloader() {
		this.asset = null;
		this.ready = false;
	}

	Preloader.prototype = {

		preload: function () {
			this.asset = this.add.sprite(320, 240, 'preloader');
			this.asset.anchor.setTo(0.5, 0.5);

			this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
			this.load.setPreloadSprite(this.asset);
			this.game.load.image('earth', 'assets/scorched_earth.png');
			this.game.load.spritesheet('player', 'assets/player.png', 50, 50);
			this.game.load.spritesheet('items', 'assets/items.png', 64, 64);
			this.game.load.image('fog', 'assets/fog.png');
			this.game.load.image('tree', 'assets/tree.png');
			this.game.load.image('grassdesert', 'assets/grassdesertmaingk1.png');
			//this.load.bitmapFont('minecraftia', 'assets/minecraftia.png', 'assets/minecraftia.xml');
		},

		create: function () {
			this.asset.cropEnabled = false;
		},

		update: function () {
			if (!!this.ready) {
				//this.game.state.start('menu');
				this.game.state.start('game');
			}
		},

		onLoadComplete: function () {
			this.ready = true;
		}
	};

	window['phaser'] = window['phaser'] || {};
	window['phaser'].Preloader = Preloader;

}());
